#include <queue>
#include <iostream>
#include <vector>
#include <map>
#include <set>
#include <stack>

using namespace std;
using State = pair<int,int>;

constexpr int CUP1_MAX = 3;
constexpr int CUP2_MAX = 5;
constexpr State I = {0,0};
constexpr State ROOT = {-1,-1};
constexpr int GOAL = 4;


// struct Solver {
//     Solver(int max1, int max2) 
//     : _cup1(max1)
//     , _cup2(max2)
//     { 
//     }

//     struct Cup {
//         Cup(int max): _garon(0), _max(max) {}
//         void empty_cup() {
//             _garon = 0;
//         }
//         void fill_cup() {
//             _garon = _max;
//         }
//         int _garon;
//         int _max;
//     };


// };

// 1 <- 2
State pour(int cup1, int cup2, int max) {
    int one, two;
    one = cup1 + cup2 > max ? max : cup1 + cup2;
    two = cup1 + cup2 > max ? cup2 + cup1 - max : 0;
    return {one, two};
}

vector<State> getNext(State st) { 
    vector<State> res;
    // 1 to 2
    res.push_back(pour(st.second, st.first, CUP1_MAX));
    // 2 to 1
    res.push_back(pour(st.first, st.second, CUP2_MAX));
    // empty1
    res.push_back({0,st.second});
    // empty2
    res.push_back({st.first, 0});
    // fill1
    res.push_back({CUP1_MAX, st.second});
    // fill2
    res.push_back({st.first, CUP2_MAX});
    return res;
}

void printState(State s, int n) {
    cout << n << ": " << "(" << s.first << ", " << s.second << ")" << endl;
}

void print_ans(map<State,State> pred, State goal) { 
    stack<State> st;
    State next = goal;

    while ( next != ROOT ) {
        st.push(next);
        next = pred[next];
    }
    int cnt = 0;
    while ( ! st.empty() ) {
        auto x = st.top();
        st.pop();
        printState(x, cnt++);
    }
}

bool isValidState(State& s) {
    return s.first >= 0 && s.second >= 0 && s.first <= CUP1_MAX && s.second <= CUP2_MAX;
}

void solve () {
    map<State,State> pred; // predecessors

    queue<State> q;
    q.push(I);
    pred[I] = ROOT;
    while (!q.empty() ) {
        State cur = q.front();
        q.pop();
        if ( (cur.first == 0 && cur.second == GOAL) 
            || (cur.first == GOAL && cur.second == 0) ) 
        {
            // final states
            print_ans(pred, cur);
            return;
        }
        vector<State> action = getNext(cur);
        for ( State next: action ) {
            if ( pred.count(next) > 0 || !isValidState(next) ) {
                continue;
            }
            pred[next] = cur;
            q.push(next);
        }
    }
    cout << "not found." << endl;
}


int main () {
    solve();
    return 0;
}
