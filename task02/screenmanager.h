#ifndef __SCREEN_MANAGER_H__
#define __SCREEN_MANAGER_H__

#include <iostream>
#include <vector>
#include "unit.h"

class CScreenManager {
public:
    CScreenManager(int N) 
    : _N(N)
    {
    }
    int _N;
    std::vector<AUnit> _units;
};

#endif
