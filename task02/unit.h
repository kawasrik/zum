#ifndef __UNIT_H__
#define __UNIT_H__

#include <iostream>
#include <memory>
#include "name.h"

using Position = std::pair<int, int>;
class CUnit {
public:
    CUnit(Position pos, char symbol) 
    : _pos (pos)
    , _symbol (symbol)
    {
    }
    virtual void print() const {
        std::cout << _symbol;
    }

    Position _pos;
    char _symbol = NSymbol::EMPTY;
};
using AUnit = std::shared_ptr<CUnit>;
#endif
