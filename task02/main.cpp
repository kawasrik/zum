/**
 * @file main.cpp
 * @author Riku Kawasaki (kawasrik@fit.cvut.cz)
 * @brief N-queens using the Hill-Climbing algorithm
 * @version 0.1
 * @date 2024-04-02
 * 
 * @copyright Copyright (c) 2024
 * 
 */

#include <iostream>
#include <set>
#include <vector>
#include <chrono>
#include <ctime>
#include <limits>
#include <thread>
#include <deque>
#include <algorithm>

int resetCnt = 0;
int sideWalkCnt = 0;
int tabuCnt = 0;

using Position = std::pair<int, int>;
Position iToPos(int i, int N) {
    int x = i % N;
    int y = i / N;
    return Position(x,y);
}
bool canAttack(const Position& p1, const Position& p2) {
    return p1.first == p2.first || p1.second == p2.second
        || abs(p1.first - p2.first) == abs(p1.second - p2.second);
}

class CState {
public:
    int evaluate() {
        if ( _value != std::numeric_limits<int>::min() ) 
            return _value;
        int res = 0;
        for ( int i = 0; i < _vec.size(); i ++ ) {
            for ( int j = i+1; j < _vec.size(); j++ ) {
                res -= canAttack(_vec[i], _vec[j]) ? 1 : 0;
            }
        }
        return _value = res;
    }
    bool goodEnough() {
        return evaluate() == 0;
    }
    int count(Position pos) const {
        int cnt = 0;
        for ( auto x : _vec ) {
            if ( x == pos ) {
                cnt ++;
            }
        }
        return cnt;
    }
    // CState getNext(int select) const {
    //     CState res;
    //     for ( int i = 0; i < _vec.size(); i ++ ) {
    //         if ( i == select ) continue;;
    //         res._vec.push_back(_vec[i]);
    //     }
    //     int tmp = rand() 
    //     Position pos;
    // }
    bool operator < (CState other) {
        return this->evaluate() < other.evaluate();
    }
    bool operator == ( CState other ) {
        return this->evaluate() == other.evaluate();
    }
    void print(int N) const {
        std::vector<char> cells (N*N, ' ');
        for ( auto x : _vec ) {
            cells[x.first+N*x.second] = 'Q';
        }
        for ( int i = 0; i < N; i ++ ) {
            for ( int j = 0; j < N; j ++ ) {
                std::cout << cells[i+j*N];
            }
            std::cout << std::endl;
        }
    }

    std::vector<Position> _vec;
    int _value = std::numeric_limits<int>::min();
};

class CTabuList {
public: 
    CTabuList (int max ) 
    : _maxSize(max){}

    void push(const std::vector<Position>& vec) {
        std::set<Position> elem;
        for ( Position pos: vec ) {
            elem.insert(pos);
        }

        auto it = std::find(_tabu.begin(), _tabu.end(), elem );
        bool found = (it != _tabu.end());
        if ( !found ) {
            if ( _tabu.size() >= (size_t) _maxSize ) {
                _tabu.pop_front();
            }
            _tabu.push_back(elem);
        }
    }
    bool exist(const std::vector<Position>& vec) const
    {
        std::set<Position> elem;
        for ( Position pos: vec ) {
            elem.insert(pos);
        }

        auto it = std::find(_tabu.begin(), _tabu.end(), elem );
        bool found = (it != _tabu.end());
        return found;
    }
    void clear () {
        _tabu.clear();
    }

    std::deque<std::set<Position>> _tabu;
    int _maxSize;
};

class COptimizer {
public:
    COptimizer (int N) 
    : _N(N)
    , _cur(getRandomState())
    , _tabu(200)
    {
        std::srand(static_cast<unsigned int>(std::time(nullptr)));
        _sideWayLim = _N*_N*5;
    }

    bool move () {
        if ( _cur.goodEnough() ) 
            return true;
        bool changed = false;
        bool canSideWayWalk = false;
        CState sideState;
        for ( int i = 0; i < _N*_N; i++ ) {
            CState candidate = getRandomNeighState();
            if ( _tabu.exist(candidate._vec)) {
                tabuCnt ++;
                continue;
            }
            _tabu.push(candidate._vec);

            if ( _cur < candidate ) {
                changed = true;
                _cur = candidate;
            } else if (_cur == candidate ) {
                canSideWayWalk = true;
                sideState = candidate;
            }
        }
        if ( ! changed ) {
            // sideway walk
            if ( _sideWayLim > 0 && canSideWayWalk) {
                _sideWayLim --;
                sideWalkCnt ++;
                _cur = sideState;
                return false;
            }
            reset();
        } 
        return false;
    }
    void print() const {
        _cur.print(_N);
    }

    int _N;
    CState _cur;
    int _sideWayLim;
    CTabuList _tabu;
private:
    void reset() {
        // printf("reset!!!\n");
        _sideWayLim = _N*_N*5;
        resetCnt ++;
        // _tabu.clear();
        _cur = getRandomState();
    }
    CState getRandomState() const {
        // Seed the random number generator with the current time
        // std::srand(static_cast<unsigned int>(std::time(nullptr)));
        int randBase = 0;
        int randRange = _N*_N - _N -randBase;
        CState res;
        for ( int i = 0; i < _N; i ++ ) {
            int tmp = randBase + (rand() % randRange);
            randBase = tmp + 1;
            randRange = _N*_N - _N + i -tmp;
            Position pos = iToPos(tmp, _N);
            // if ( res.count(pos) > 0 ) throw std::runtime_error("error");
            res._vec.push_back(pos);
        }
        res.evaluate();
        return res;
    }

    // CState getNeighbors() const {
    // }
    CState getRandomNeighState() const {
        // std::srand(static_cast<unsigned int>(std::time(nullptr)));
        int select = rand() % _N;
        CState res;
        for ( int i = 0; i < _cur._vec.size(); i ++ ) {
            if ( i == select ) continue;
            res._vec.push_back(_cur._vec[i]);
        }
        Position pos = _cur._vec[select];
        switch (rand() % 8 ) {
            case 0: 
                pos.second = (pos.second+1) %_N;
                break;
            case 1: 
                pos.second = (pos.second-1 + _N) %_N;
                break;
            case 2: 
                pos.first = (pos.first+1) %_N;
                break;
            case 3: 
                pos.first = (pos.first-1 + _N) %_N;
                break;
            case 4: 
                pos.second = (pos.second+1) %_N;
                pos.first = (pos.first+1) %_N;
                break;
            case 5: 
                pos.second = (pos.second-1 + _N) %_N;
                pos.first = (pos.first+1) %_N;
                break;
            case 6: 
                pos.second = (pos.second-1 + _N) %_N;
                pos.first = (pos.first-1 + _N) %_N;
                break;
            case 7: 
                pos.second = (pos.second+1) %_N;
                pos.first = (pos.first-1 + _N) %_N;
                break;
            default: break;
        }
        // Position pos = iToPos(rand() % (_N*_N), _N);
        // while ( _cur.count(pos) != 0 ) {
        //     pos = iToPos(rand() % (_N*_N), _N);
        // }
        res._vec.push_back(pos);
        res.evaluate();
        return res;
    }
};

void flashScreen() {
    std::cout << "\033[2J\033[1;1H";
}

int main () 
{
    int N;
    std::cout << "Type N:" << std::endl;
    std::cin >> N;
    COptimizer opt(N);
    while ( !opt._cur.goodEnough() ) {
        flashScreen();
        opt.print();
        std::cout << "N: " << N << std::endl;
        std::cout << "value: " << opt._cur._value << std::endl;
        std::cout << "sidewalk count: " << sideWalkCnt << std::endl;
        std::cout << "reset count: " << resetCnt << std::endl;
        std::cout << "tabu block count: " << tabuCnt << std::endl;
        // std::this_thread::sleep_for(std::chrono::milliseconds(1) );
        std::this_thread::sleep_for(std::chrono::nanoseconds(1) );
        opt.move();
    }
    flashScreen();
    opt.print();
    std::cout << "N: " << N << std::endl;
    std::cout << "value: " << opt._cur._value << std::endl;
    std::cout << "sidewalk count: " << sideWalkCnt << std::endl;
    std::cout << "reset count: " << resetCnt << std::endl;
    std::cout << "tabu block count: " << tabuCnt << std::endl;
    return 0;
}
