(define (problem pacman1) 
(:domain pacman)
(:requirements :strips)
(:objects 
    pacman1
    l1 l2 l3 l4 l5 l6 l7)

(:init 
    (adjacent l1 l2)
    (adjacent l2 l3)
    (adjacent l2 l4)
    (adjacent l4 l6)
    (adjacent l5 l6)
    (adjacent l6 l7)
    
    (adjacent l2 l1)
    (adjacent l3 l2)
    (adjacent l4 l2)
    (adjacent l6 l4)
    (adjacent l6 l5)
    (adjacent l7 l6)
    
    (at pacman1 l4)
)
  
(:goal 
    (and (at pacman1 l1)) 
)

)
