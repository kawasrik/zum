; Two pacmen (Collision free)
(define (domain pacman)
(:requirements :strips)

(:predicates
    (adjacent ?l1 ?l2)
    (at ?packman ?l)
    (nopill ?l)
)

(:action move
:parameters(?pacman1 ?pacman2 ?from ?to)
:precondition(and (adjacent ?from ?to)
                  (at ?pacman1 ?from) 
                  (not (at ?pacman2 ?to)))
:effect (and (at ?pacman1 ?to)
        (not (at ?pacman1 ?from)) 
        )
)

(:action eatPill
:parameters(?pacman ?l )
:precondition(and (at ?pacman ?l)
                (not (nopill ?l) )
            )
:effect (and (nopill ?l))
)

)
