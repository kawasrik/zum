(define (domain pacman)
(:requirements :strips)

(:predicates
    (adjacent ?l1 ?l2)
    (at ?packman ?l)
    (nopill ?l)
)

(:action move
:parameters(?pacman ?from ?to)
:precondition(and (adjacent ?from ?to)
                  (at ?pacman ?from) )
:effect (and (at ?pacman ?to)
        (not (at ?pacman ?from)) 
        )
)

(:action eatPill
:parameters(?pacman ?l )
:precondition(and (at ?pacman ?l)
                (not (nopill ?l) )
            )
:effect (and (nopill ?l))
)

)
