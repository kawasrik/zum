/**
 * @file hanoi_st.cpp
 * @author Riku Kawasaki (kawasrik@fit.cvut.cz)
 * @brief solve hanoi's tower problem in the standard way
 *        There are 3 poles {0, 1, 2}. n disks are piled at the pole0.
 *        For all the disks, you cannot put the larger disks on the smaller one.
 *        The task is moving all the disk from pole0 to pole2.
 * @version 0.1
 * @date 2024-03-01
 * 
 * @copyright Copyright (c) 2024
 * 
 */
#include <iostream>
#include <vector>
#include <stack>
#include <exception>
constexpr int NUM_POLES = 3;
using namespace std;
using Disk = int;

struct Problem {
    Problem(int n)
    : _poles(NUM_POLES)
    , _cnt ( 0 )
    {
        for ( int i = 0; i < n; i ++ ) {
            _poles[0].push(n-i);
        }
    }
    void solve(int src, int mid, int dst, int n) {
        if ( n == 1 ) {
            int tmp = _poles[src].top();
            _poles[src].pop();
            // debug
            if ( !_poles[dst].empty() && tmp >= _poles[dst].top() ) {
                throw std::logic_error("attempted to put larger disk.");
            }
            _poles[dst].push(tmp);
            cout << "disk" << tmp << ": " << src << " -> " << dst << endl;
            _cnt ++;
            return;
        }
        solve(src, dst, mid, n-1);
        solve(src, mid, dst, 1);
        solve(mid, src, dst, n-1);
    }
    vector<stack<Disk>> _poles;
    size_t _cnt;
};

int main () {
    int n;
    cout << "Type the number of disks:" << endl;
    cin >> n;
    Problem p(n);
    p.solve(0, 1, 2, n);
    cout << "The number of moves: " << p._cnt << endl;
    return 0;
}
