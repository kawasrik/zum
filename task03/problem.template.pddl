; Lloyd’s 15 Puzzle
(define (problem puzzle1)
(:domain puzzle)
(:requirements :strips)
(:objects 
    b1 b2 b3
    g1 g2 g3 g4
)

(:init
    (adjacent g1 g2)
    (adjacent g1 g3)
    (adjacent g2 g4)
    (adjacent g3 g4)
    
    (adjacent g2 g1)
    (adjacent g3 g1)
    (adjacent g4 g2)
    (adjacent g4 g3)

    (at b1 g2)
    (at b2 g4)
    (at b3 g1)
    (missing g3)
)
  
(:goal 
    (and
    (at b1 g1)
    (at b2 g2)
    (at b3 g3)
    (missing g4)
    )
)


)
