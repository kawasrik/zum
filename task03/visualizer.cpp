#include <iostream>
#include <iomanip>
#include <fstream>
#include <vector>
#include <string>
#include <thread>
#include <chrono>

using namespace std;

const char* planFile = "plan.txt";
const char* boardFile = "initialBoard.txt";


void flashScreen() {
    std::cout << "\033[2J\033[1;1H";
}

void printBoard(const vector<int>& vec, int N ) {
    int width = N*N / 10 + 2;
    for (int i = 0; i < N*N; i ++ ) {
        if ( vec[i] == N*N-1) {
            cout << setw(width) << "*";
        } else {
            cout << setw(width) << vec[i];
        }
        if ( i % N == N-1 ) {
            cout << endl;
        }
    }
}

int main() {
    ifstream ifsB (boardFile);
    if (!ifsB) {
        cout << boardFile << " is not opedned.\n";
        return 1;
    }
    vector<int> vec;
    int N;
    if ( !(ifsB >> N) || N <= 0) {
        cout << "N is not valid" << endl;
        return 1;
    }
    cout << "N is " << N << endl;
    // read the initial board
    for ( int i = 0; i < N*N; i ++ ) {
        int tmp = 0;
        if ( ! (ifsB >> tmp) ) {
            cout << "couldn't read the block number" << endl;
            return 1;
        }
        vec.push_back(tmp);
    }
    ifsB.close();

    ifstream ifsP (planFile);
    if ( !ifsP ) {
        cout << planFile << " is not opened\n";
        return 1;
    }
    string line;
    int cnt = 0;
    while ( getline(ifsP, line) ) {
        flashScreen();
        printBoard(vec, N);
        printf ("cnt: %d\n", cnt++);
        int block, from, to;
        if ( sscanf(line.c_str(), "(move b%d g%d g%d)", &block, &from, &to) != 3 )
        {
            cout << "Syntax error in " << planFile << endl;
            return 1;
        }
        // printf ("from: %d\nto: %d\n", from, to);
        swap(vec[from], vec[to]);
        this_thread::sleep_for(chrono::milliseconds(200));
    }
    flashScreen();
    printBoard(vec, N);
    printf ("cnt: %d\n", cnt++);

    return 0;
}
