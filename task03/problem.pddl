; Lloyd’s 15 Puzzle
(define
(problem puzzle1)
(:domain puzzle)
(:requirements :strips)
(:objects
; (N*N-1) blocks
	b0
	b1
	b2
	b3
	b4
	b5
	b6
	b7
	b8
	b9
	b10
	b11
	b12
	b13
	b14
; N*N grids
	g0
	g1
	g2
	g3
	g4
	g5
	g6
	g7
	g8
	g9
	g10
	g11
	g12
	g13
	g14
	g15
)
(:init
	(adjacent g0 g4)
	(adjacent g0 g1)
	(adjacent g1 g5)
	(adjacent g1 g0)
	(adjacent g1 g2)
	(adjacent g2 g6)
	(adjacent g2 g1)
	(adjacent g2 g3)
	(adjacent g3 g7)
	(adjacent g3 g2)
	(adjacent g4 g0)
	(adjacent g4 g8)
	(adjacent g4 g5)
	(adjacent g5 g1)
	(adjacent g5 g9)
	(adjacent g5 g4)
	(adjacent g5 g6)
	(adjacent g6 g2)
	(adjacent g6 g10)
	(adjacent g6 g5)
	(adjacent g6 g7)
	(adjacent g7 g3)
	(adjacent g7 g11)
	(adjacent g7 g6)
	(adjacent g8 g4)
	(adjacent g8 g12)
	(adjacent g8 g9)
	(adjacent g9 g5)
	(adjacent g9 g13)
	(adjacent g9 g8)
	(adjacent g9 g10)
	(adjacent g10 g6)
	(adjacent g10 g14)
	(adjacent g10 g9)
	(adjacent g10 g11)
	(adjacent g11 g7)
	(adjacent g11 g15)
	(adjacent g11 g10)
	(adjacent g12 g8)
	(adjacent g12 g13)
	(adjacent g13 g9)
	(adjacent g13 g12)
	(adjacent g13 g14)
	(adjacent g14 g10)
	(adjacent g14 g13)
	(adjacent g14 g15)
	(adjacent g15 g11)
	(adjacent g15 g14)
	(at b2 g0)
	(at b11 g1)
	(at b8 g2)
	(at b3 g3)
	(at b4 g4)
	(at b10 g5)
	(at b12 g6)
	(at b6 g7)
	(at b5 g8)
	(at b9 g9)
	(missing g10)
	(at b1 g11)
	(at b7 g12)
	(at b13 g13)
	(at b14 g14)
	(at b0 g15)
)
(:goal
	(and
	(at b0 g0)
	(at b1 g1)
	(at b2 g2)
	(at b3 g3)
	(at b4 g4)
	(at b5 g5)
	(at b6 g6)
	(at b7 g7)
	(at b8 g8)
	(at b9 g9)
	(at b10 g10)
	(at b11 g11)
	(at b12 g12)
	(at b13 g13)
	(at b14 g14)
	(missing g15)
	)
)
)
