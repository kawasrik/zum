; Lloyd’s 15 Puzzle
(define (domain puzzle)
(:requirements :strips)

(:predicates
    (adjacent ?grid1 ?grid2)
    (missing ?grid)
    (at ?block ?grid2)
)

(:action move
    :parameters(?block ?from ?to)
    :precondition(and 
        (at ?block ?from)
        (adjacent ?from ?to)
        (missing ?to)
    )
    :effect (and 
        (not (at ?block ?from))
        (at ?block ?to )
        (not (missing ?to))
        (missing ?from)
    )
)

)
