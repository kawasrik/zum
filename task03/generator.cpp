/**
 * @file generator.cpp
 * @author Riku Kawasaki (kawasrik@fit.cvut.cz)
 * @brief This program generates problem.pddl file
 * @version 0.1
 * @date 2024-04-13
 * 
 * @copyright Copyright (c) 2024
 * 
 */

#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <sstream>
#include <cstdlib>
#include <ctime>
#include <vector>

const char* outputFileName = "problem.pddl";
using namespace std;


const char* domainStr = "(:domain puzzle)\n";
const char* requirementsStr = "(:requirements :strips)\n";
stringstream objectsStr(int N) {
    const char* header = "(:objects\n";
    stringstream res;
    res << header;
    // Put (N*N-1) blocks
    res << "; (N*N-1) blocks" << endl;
    for ( int i = 0; i < N*N-1; i ++ ) {
        res << "\tb" << i << endl;
    }
    // Put N*N grids
    res << "; N*N grids" << endl;
    for ( int i = 0; i < N*N; i ++ ) {
        res << "\tg" << i << endl;
    }
    res << ")\n";
    return res;
}

#define UP (row*N + col -N)
#define DOWN (row*N + col +N)
#define RIGHT (row*N + col +1)
#define LEFT (row*N + col -1)

stringstream initStr(int N) {
    const char* header = "(:init\n";
    stringstream res;
    res << header;
    // adjacent
    for ( int row = 0; row < N; row ++ ) {
        for ( int col = 0; col < N; col ++ ) {
            if (row != 0) {
                res << "\t(adjacent g" << row*N + col << " g" << UP << ")\n";
            }
            if (row != N-1) {
                res << "\t(adjacent g" << row*N + col << " g" << DOWN << ")\n";
            }
            if (col != 0) {
                res << "\t(adjacent g" << row*N + col << " g" << LEFT << ")\n";
            }
            if (col != N-1) {
                res << "\t(adjacent g" << row*N + col << " g" << RIGHT << ")\n";
            }
        }
    }
    // at
    vector<int> vec;
    for ( int i = 0; i < N*N; i ++ ) {
        vec.push_back(i);
    }
    for ( int i = 0; i < N*N/2; i ++ ) {
        int idx1 = rand()%(N*N);
        int idx2 = rand()%(N*N);
        swap(vec[idx1], vec[idx2]);
    }
    // place blocks
    ofstream ofs ("initialBoard.txt");
    if ( ! ofs ) exit(1);
    ofs << N << endl;
    int width = N*N / 10 + 2;
    for ( int i = 0; i < N*N; i ++ ) {
        if ( vec[i] == N*N-1 ) {
            res << "\t(missing g" << i << ")\n";
        } else {
            res << "\t(at b" << vec[i] << " g" << i << ")\n";
        }
        
        ofs << setw(width) << vec[i];
        if ( i % N == N-1 ) {
            ofs << endl;
        }
    }

    res << ")\n";
    return res;
}
stringstream goalStr(int N) {
    const char* header = "(:goal\n\t(and\n";
    stringstream res;
    res << header;
    for ( int i = 0; i < N*N-1; i ++ ) {
        res << "\t(at b" << i << " g" << i << ")\n";
    }
    res << "\t(missing g" << N*N-1 << ")\n";
    res << "\t)\n)\n";
    return res;
}
stringstream defineStr(int N)
{
    const char* header =
    "; Lloyd's 15 Puzzle\n(define\n(problem puzzle1)\n";
    stringstream res;
    res << header;
    // domain
    res << domainStr;
    // requirements
    res << requirementsStr;
    // objects
    res << objectsStr(N).str();
    // init
    res << initStr(N).str();
    // goal
    res << goalStr(N).str();
    res << ")\n";
    return res;
}

int main (int argc, char * argv []) 
{
    srand(static_cast<unsigned>(time(nullptr)));
    int N;
    if ( argc != 2 
        || sscanf(argv[1], "%d", &N) != 1)
    {
        cout << "Usage: " << argv[0] << " <N>" << endl;
        return 1;
    }
    if ( N <= 0 ) {
        cout << "N should be natural number." << endl;
        return 1;
    }

    ofstream ofs(outputFileName);
    if ( ! ofs ) {
        cout << outputFileName << " is not open." << endl;
        return 1;
    }
    ofs << defineStr(N).str();

    return 0;
}
